package org.hurst.bot;

import com.google.common.collect.*;
import net.dv8tion.jda.entities.*;

import java.util.*;

/**
 * Created by tyler on 12/17/2016.
 */
public class Config {
    private String api_key;
    private String mainChannel;
    private String designatedBotSpamChannel;
    private String defaultTwitchChannel;
    private String botMentionTag;
    private String bannedFromBotRoleName;
    private String welcomeMessage;
    private String farewellMessage;
    private int defaultHourRestrictionPunishment = 2;
    private final List<String> managerialRoles = Lists.newLinkedList();
    private final List<String> parodyGameStatusList = Lists.newLinkedList();
    private final List<String> jokeNudesImageList = Lists.newLinkedList();
    private final List<String> randomMemeImageList = Lists.newLinkedList();
    private final List<String> randomJokeList = Lists.newLinkedList();

    public boolean containsManagerialRole(List<Role> roles) {
        for (Role role : roles) {
            for (String managerialRole : managerialRoles) {
                if (role.getName().equalsIgnoreCase(managerialRole))
                    return true;
            }
        }
        return false;
    }

    public List<String> getManagerialRoles() {
        return managerialRoles;
    }

    public String getDesignatedBotSpamChannel() {
        return designatedBotSpamChannel;
    }

    public int getDefaultHourRestrictionPunishment() {
        return defaultHourRestrictionPunishment;
    }

    public String getMainChannel() {
        return mainChannel;
    }

    public List<String> getParodyGameStatusList() {
        return parodyGameStatusList;
    }

    public List<String> getJokeNudesImageList() {
        return jokeNudesImageList;
    }

    public List<String> getRandomMemeImageList() {
        return randomMemeImageList;
    }

    public List<String> getRandomJokeList() {
        return randomJokeList;
    }

    public String getApiKey() {
        return api_key;
    }

    public String getDefaultTwitchChannel() {
        return defaultTwitchChannel;
    }

    public String getBotMentionTag() {
        return botMentionTag;
    }

    public String getBannedFromBotRoleName() {
        return bannedFromBotRoleName;
    }

    public String getApi_key() {
        return api_key;
    }

    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    public String getFarewellMessage() {
        return farewellMessage;
    }
}
