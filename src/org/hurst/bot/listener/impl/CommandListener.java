package org.hurst.bot.listener.impl;

import net.dv8tion.jda.events.message.*;
import org.hurst.bot.listener.*;
import org.hurst.discord.command.*;
import org.hurst.discord.command.CommandRepository.*;
import org.hurst.util.*;

import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

/**
 * Created by tyler on 12/14/2016.
 */
public class CommandListener extends BotListener {
    private static final Logger logger = Logger.createDefaultLogger();
    private static final Pattern PATTERN_ON_SPACE = Pattern.compile(" ", Pattern.LITERAL);
    private static final String[] ACCEPTED_IDENTIFIERS = {"!", "\\"};
    private final CommandRepository repository = new CommandRepository();
    private final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        String message = event.getMessage().getContent();
        if (message.isEmpty() || event.getAuthor().isBot())
            return;
        if (bot.getMember(event.getAuthor()).hasRole(bot.getConfig().getBannedFromBotRoleName())) {
            event.getAuthor().getPrivateChannel().sendMessage("Sorry, you've been banned from using me on " + bot.getServer().getName());
            return;
        }

        String mention = "@" + event.getJDA().getSelfInfo().getUsername() + " ";
        if (message.startsWith(mention)) {
            handleCommand(message.trim().replace(mention, "!"), event);
            return;
        }

        for (String identifier : ACCEPTED_IDENTIFIERS) {
            if (message.startsWith(identifier))
                handleCommand(message.trim().replace(identifier + " ", "!"), event);
        }
    }

    private void handleCommand(String message, MessageReceivedEvent event) {
        String cmd = message.split(" ")[0].replace("/\n/g", " ").substring(1).toLowerCase();
        String[] args = PATTERN_ON_SPACE.split(message);
        args = Arrays.copyOfRange(args, 1, args.length);
        Command command = repository.get(cmd);

        service.schedule(() -> {
            event.getMessage().deleteMessage();
        }, 5, TimeUnit.SECONDS);

        if (command == null) {
            logger.warn("Couldn't find command: {}", message);
            logger.debug(cmd);
            logger.debug(Arrays.toString(args));
            return;
        }
        if (command.isManagerialCommand() && !bot.getConfig().containsManagerialRole(bot.getServer().getRolesForUser(event.getAuthor()))) {
            event.getChannel().sendMessage(event.getAuthor().getAsMention() + " You do not have permissions to use this command.");
            return;
        }
        command.execute(bot, bot.getMember(event.getAuthor()), event.getChannel(), event.getMessage(), args);
    }
}
