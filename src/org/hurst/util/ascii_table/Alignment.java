package org.hurst.util.ascii_table;

public enum Alignment {
    LEFT, CENTER, RIGHT;
    public static final Alignment HEADER_DEFAULT = LEFT;
    public static final Alignment DATA_DEFAULT = LEFT;
}