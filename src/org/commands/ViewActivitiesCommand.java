package commands;

import net.dv8tion.jda.entities.*;
import org.hurst.bot.*;
import org.hurst.discord.command.CommandRepository.*;
import org.hurst.discord.member.*;
import org.hurst.util.*;
import org.hurst.util.ascii_table.*;

import java.util.*;

import static org.hurst.util.StringUtil.*;

/**
 * Created by tyler on 12/15/2016.
 */
public class ViewActivitiesCommand extends Command {

    public ViewActivitiesCommand() {
        super("activities", "View the activities leaderboard entry of another RS3 player");
    }

    @Override
    public void execute(DiscordBot bot, Member member, MessageChannel channel, Message message, String[] arguments) {
        if (arguments.length == 0) {
            channel.sendMessage(member.getUser().getAsMention() + ", you need to specify a user who's stats you wish to lookup!");
            return;
        }
        String displayName = "";
        for (int i = 0; i < arguments.length; i++) {
            displayName += arguments[i] + " ";
        }
        displayName = StringUtil.formatDisplayName(displayName.trim());
        try {
            List<String> lines = WebUtil.readAllLines("http://services.runescape.com/m=hiscore/index_lite.ws?player=" + displayName.toLowerCase());
            if (lines == null || lines.size() < 50) {
                channel.sendMessage(member.getUser().getAsMention() + ", lines are fucked? " + lines.size());
                return;
            }
            StringBuilder builder = new StringBuilder();
            builder.append("Viewing activities for " + displayName);
            String[] overall = lines.get(0).split(",");
            builder.append("\nOverall rank: " + format(overall[0]));
            builder.append("\nTotal level: " + format(overall[1]));
            builder.append("\nTotal experience: " + format(overall[2]));

            List<ActivityEntry> activityEntries = Arrays.asList(new ActivityEntry("Bounty Hunters", lines.get(28).split(",")),
                    new ActivityEntry("BH Rogues", lines.get(29).split(",")),
                    new ActivityEntry("Dominion Tower", lines.get(30).split(",")),
                    new ActivityEntry("The Crucible", lines.get(31).split(",")),
                    new ActivityEntry("Castle Wars Games", lines.get(32).split(",")),
                    new ActivityEntry("B.A Attackers", lines.get(33).split(",")),
                    new ActivityEntry("B.A Defenders", lines.get(34).split(",")),
                    new ActivityEntry("B.A Collectors", lines.get(35).split(",")),
                    new ActivityEntry("B.A Healers", lines.get(36).split(",")),
                    new ActivityEntry("Duel Tournaments", lines.get(37).split(",")),
                    new ActivityEntry("Mobilising Armies", lines.get(38).split(",")),
                    new ActivityEntry("Conquest", lines.get(39).split(",")),
                    new ActivityEntry("Fist of Guthix", lines.get(40).split(",")),
                    new ActivityEntry("GG: Resource Race", lines.get(41).split(",")),
                    new ActivityEntry("GG: Athletics", lines.get(42).split(",")),
                    new ActivityEntry("WE2: Armadyl Contribution", lines.get(43).split(",")),
                    new ActivityEntry("WE2: Bandos Contribution", lines.get(44).split(",")),
                    new ActivityEntry("WE2: Armadyl PvP Kills", lines.get(45).split(",")),
                    new ActivityEntry("WE2: Bandos PvP Kills", lines.get(46).split(",")),
                    new ActivityEntry("Heist Guard Level", lines.get(47).split(",")),
                    new ActivityEntry("Heist Robber Level", lines.get(48).split(",")),
                    new ActivityEntry("CFP: 5 Game Average", lines.get(49).split(",")));

            String activityTable = Table.getTable(activityEntries,
                    Arrays.asList(new Column("Activity").with(p -> p.name),
                            new Column("Rank").with(p -> p.array[0].equals("-1") ? "unranked" : format(p.array[0])),
                            new Column("Total").with(p -> p.array[1].equals("-1") ? "unranked" : format(p.array[1]))));
            builder.append("\n" + activityTable);

            channel.sendMessage(BotUtil.orange(builder.toString()));

        } catch (Exception e) {
            channel.sendMessage(member.getUser().getAsMention() + ", I've had an issue looking up the stats for that user, make sure this username is " +
                    "correct.");
            e.printStackTrace();
        }

    }

    public static class StatEntry {
        final String name;
        final String[] array;

        public StatEntry(String name, String[] array) {
            this.name = name;
            this.array = array;
        }
    }

    public static class ActivityEntry {
        final String name;
        final String[] array;

        public ActivityEntry(String name, String[] array) {
            this.name = name;
            this.array = array;
        }
    }

    public int getLevelForXp(String skill, int exp) {
        int points = 0;
        int output = 0;
        for (int lvl = 1; lvl <= (120); lvl++) {
            points += Math.floor(lvl + 300.0 * Math.pow(2.0, lvl / 7.0));
            output = (int) Math.floor(points / 4);
            if ((output - 1) >= exp) {
                return lvl;
            }
        }
        return 120;
    }

    public int getXPForLevel(int level) {
        int points = 0;
        int output = 0;
        for (int lvl = 1; lvl <= level; lvl++) {
            points += (int) Math.floor((double) lvl + 300.0f * Math.pow(2.0f, (double) lvl / 7.0f));
            if (lvl >= level)
                return output;
            output = (int) Math.ceil((double) points / 4);
        }
        return 0;
    }

    public enum Skills {
        ATTACK, DEFENCE, STRENGTH, CONSTITUTION, RANGED, PRAYER, MAGIC, COOKING, WOODCUTTING, FLETCHING, FISHING, FIREMAKING, CRAFTING, SMITHING, MINING,
        HERBLORE, AGILITY, THIEVING, SLAYER, FARMING, RUNECRAFTING, CONSTRUCTION, HUNTER, SUMMONING, DUNGEONEERING, DIVINATION, INVENTION;

        public static String getName(int id) {
            for (Skills skills : values()) {
                if (id == skills.ordinal())
                    return StringUtil.formatDisplayName(skills.name());
            }
            return "unknown";
        }
    }
}
