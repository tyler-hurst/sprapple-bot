package commands;

import net.dv8tion.jda.entities.*;
import org.hurst.bot.*;
import org.hurst.discord.command.CommandRepository.*;
import org.hurst.discord.member.*;
import org.hurst.util.*;
import org.hurst.util.ascii_table.*;

import java.util.*;

import static org.hurst.util.StringUtil.*;

/**
 * Created by tyler on 12/15/2016.
 */
public class ViewStatsCommand extends Command {

    public ViewStatsCommand() {
        super("stats", "View the stats leaderboard entry of another RS3 player");
    }

    @Override
    public void execute(DiscordBot bot, Member member, MessageChannel channel, Message message, String[] arguments) {
        if (arguments.length == 0) {
            channel.sendMessage(member.getUser().getAsMention() + ", you need to specify a user who's stats you wish to lookup!");
            return;
        }
        String displayName = "";
        for (int i = 0; i < arguments.length; i++) {
            displayName += arguments[i] + " ";
        }
        displayName = StringUtil.formatDisplayName(displayName.trim());
        try {
            List<String> lines = WebUtil.readAllLines("http://services.runescape.com/m=hiscore/index_lite.ws?player=" + displayName.toLowerCase());
            if (lines == null || lines.size() < 50) {
                channel.sendMessage(member.getUser().getAsMention() + ", lines are fucked? " + lines.size());
                return;
            }
            StringBuilder builder = new StringBuilder();
            builder.append("Viewing stats for " + displayName);
            String[] overall = lines.get(0).split(",");
            builder.append("\nOverall rank: " + format(overall[0]));
            builder.append("\nTotal level: " + format(overall[1]));
            builder.append("\nTotal experience: " + format(overall[2]));

            List<StatEntry> statEntries = Arrays.asList(new StatEntry("Attack", lines.get(1).split(",")),
                    new StatEntry("Defence", lines.get(2).split(",")),
                    new StatEntry("Strength", lines.get(3).split(",")),
                    new StatEntry("Consitution", lines.get(4).split(",")),
                    new StatEntry("Ranged", lines.get(5).split(",")),
                    new StatEntry("Prayer", lines.get(6).split(",")),
                    new StatEntry("Magic", lines.get(7).split(",")),
                    new StatEntry("Cooking", lines.get(8).split(",")),
                    new StatEntry("Woodcutting", lines.get(9).split(",")),
                    new StatEntry("Fletching", lines.get(10).split(",")),
                    new StatEntry("Fishing", lines.get(11).split(",")),
                    new StatEntry("Firemaking", lines.get(12).split(",")),
                    new StatEntry("Crafting", lines.get(13).split(",")),
                    new StatEntry("Smithing", lines.get(14).split(",")),
                    new StatEntry("Mining", lines.get(15).split(",")),
                    new StatEntry("Herblore", lines.get(16).split(",")),
                    new StatEntry("Agility", lines.get(17).split(",")),
                    new StatEntry("Thieving", lines.get(18).split(",")),
                    new StatEntry("Slayer", lines.get(19).split(",")),
                    new StatEntry("Farming", lines.get(20).split(",")),
                    new StatEntry("Runecrafting", lines.get(21).split(",")),
                    new StatEntry("Construction", lines.get(22).split(",")),
                    new StatEntry("Hunter", lines.get(23).split(",")),
                    new StatEntry("Summoning", lines.get(24).split(",")),
                    new StatEntry("Dungeoneering", lines.get(25).split(",")),
                    new StatEntry("Divination", lines.get(26).split(",")),
                    new StatEntry("Invention", lines.get(27).split(",")),
                    new StatEntry(RandomUtil.get(Arrays.asList("Bankstanding", "Sailing", "Elitism")), new String[]{"1", "120", "200000000"}));

            String table = Table.getTable(statEntries,
                    Arrays.asList(new Column("Skill").with(p -> p.name),
                            new Column("Level").with(p -> format(p.array[1]) + (!p.name.equals("Invention") && !p.name.equals("Dungeoneering") &&
                                    getLevelForXp(
                                    p.name,
                                    Integer.parseInt(p.array[2])) != Integer.parseInt(p.array[1]) ? " " + "(" + getLevelForXp(p.name,
                                    Integer.parseInt(p.array[2])) + ")" : "")),
                            new Column("Experience").with(p -> format(p.array[2])),
                            new Column("Rank").with(p -> format(p.array[0]))));
            builder.append("\n" + table);
            channel.sendMessage(BotUtil.green(builder.toString()));

        } catch (Exception e) {
            channel.sendMessage(member.getUser().getAsMention() + ", I've had an issue looking up the stats for that user, make sure this username is " +
                    "correct.");
            e.printStackTrace();
        }

    }

    public static class StatEntry {
        final String name;
        final String[] array;

        public StatEntry(String name, String[] array) {
            this.name = name;
            this.array = array;
        }
    }

    public static class ActivityEntry {
        final String name;
        final String[] array;

        public ActivityEntry(String name, String[] array) {
            this.name = name;
            this.array = array;
        }
    }

    public int getLevelForXp(String skill, int exp) {
        int points = 0;
        int output = 0;
        for (int lvl = 1; lvl <= (120); lvl++) {
            points += Math.floor(lvl + 300.0 * Math.pow(2.0, lvl / 7.0));
            output = (int) Math.floor(points / 4);
            if ((output - 1) >= exp) {
                return lvl;
            }
        }
        return 120;
    }

    public int getXPForLevel(int level) {
        int points = 0;
        int output = 0;
        for (int lvl = 1; lvl <= level; lvl++) {
            points += (int) Math.floor((double) lvl + 300.0f * Math.pow(2.0f, (double) lvl / 7.0f));
            if (lvl >= level)
                return output;
            output = (int) Math.ceil((double) points / 4);
        }
        return 0;
    }

    public enum Skills {
        ATTACK, DEFENCE, STRENGTH, CONSTITUTION, RANGED, PRAYER, MAGIC, COOKING, WOODCUTTING, FLETCHING, FISHING, FIREMAKING, CRAFTING, SMITHING, MINING,
        HERBLORE, AGILITY, THIEVING, SLAYER, FARMING, RUNECRAFTING, CONSTRUCTION, HUNTER, SUMMONING, DUNGEONEERING, DIVINATION, INVENTION;

        public static String getName(int id) {
            for (Skills skills : values()) {
                if (id == skills.ordinal())
                    return StringUtil.formatDisplayName(skills.name());
            }
            return "unknown";
        }
    }
}
