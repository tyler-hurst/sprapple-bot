package org.hurst.util.ascii_table;

import java.util.*;
import java.util.stream.*;

public class Table {
    public static <T> String getTable(Collection<T> objects, List<Column.Data<T>> columns) {
        String[][] data = new String[objects.size()][];
        Iterator<T> iter = objects.iterator();
        int i = 0;
        while (i < objects.size()) {
            T object = iter.next();
            data[i] = new String[columns.size()];
            for (int j = 0; j < columns.size(); ++j)
                data[i][j] = columns.get(j).getter.apply(object);
            ++i;
        }
        Column[] rawColumns = columns.stream().map(c -> c.column).collect(Collectors.toList()).toArray(new Column[columns.size()]);
        return getTable(rawColumns, data);
    }

    public static String getTable(String[] header, String[][] data) {
        Column[] headerCol = Arrays.stream(header).map(Column::new).collect(Collectors.toList()).toArray(new Column[header.length]);
        return getTable(headerCol, data);
    }

    private static String getTable(Column[] headerObjs, String[][] data) {
        if (data == null || data.length == 0)
            throw new IllegalArgumentException("Please provide valid data : " + Arrays.deepToString(data));
        StringBuilder tableBuf = new StringBuilder();
        String[] header = getHeaders(headerObjs);
        int colCount = getMaxColumns(header, data);

        List<Integer> colMaxLenList = getMaxColLengths(colCount, header, data);
        if (header != null && header.length > 0) {
            tableBuf.append(getRowLineBuf(colCount, colMaxLenList, data));
            tableBuf.append(getRowDataBuf(colCount, colMaxLenList, header, headerObjs, true));
        }

        tableBuf.append(getRowLineBuf(colCount, colMaxLenList, data));
        String[] rowData = null;

        for (String[] aData : data) {
            rowData = new String[colCount];
            for (int j = 0; j < colCount; j++) {
                if (j < aData.length)
                    rowData[j] = aData[j];
                else
                    rowData[j] = "";
            }
            tableBuf.append(getRowDataBuf(colCount, colMaxLenList, rowData, headerObjs, false));
        }

        tableBuf.append(getRowLineBuf(colCount, colMaxLenList, data));
        return tableBuf.toString();
    }

    private static String getRowDataBuf(int colCount, List<Integer> colMaxLenList, String[] row, Column[] headerObjs, boolean isHeader) {
        StringBuilder rowBuilder = new StringBuilder();
        String formattedData = null;
        Alignment alignment;
        for (int i = 0; i < colCount; i++) {
            alignment = isHeader ? Alignment.HEADER_DEFAULT : Alignment.DATA_DEFAULT;
            if (headerObjs != null && i < headerObjs.length) {
                if (isHeader)
                    alignment = headerObjs[i].headerAlignment;
                else
                    alignment = headerObjs[i].dataAlignment;
            }
            formattedData = i < row.length ? row[i] : "";
            formattedData = "| " + getFormattedData(colMaxLenList.get(i), formattedData, alignment) + " ";
            if (i + 1 == colCount)
                formattedData += "|";
            rowBuilder.append(formattedData);
        }
        return rowBuilder.append("\n").toString();
    }

    private static String getFormattedData(int maxLength, String data, Alignment alignment) {
        if (data.length() > maxLength)
            return data;
        boolean toggle = true;
        while (data.length() < maxLength) {
            if (alignment == Alignment.LEFT)
                data = data + " ";
            else if (alignment == Alignment.RIGHT)
                data = " " + data;
            else if (alignment == Alignment.CENTER) {
                if (toggle) {
                    data = " " + data;
                    toggle = false;
                } else {
                    data = data + " ";
                    toggle = true;
                }
            }
        }
        return data;
    }

    private static String getRowLineBuf(int colCount, List<Integer> colMaxLenList, String[][] data) {
        StringBuilder rowBuilder = new StringBuilder();
        int colWidth = 0;
        for (int i = 0; i < colCount; i++) {
            colWidth = colMaxLenList.get(i) + 3;
            for (int j = 0; j < colWidth; j++) {
                if (j == 0)
                    rowBuilder.append("+");
                else if ((i + 1 == colCount && j + 1 == colWidth))
                    rowBuilder.append("-+");
                else
                    rowBuilder.append("-");
            }
        }
        return rowBuilder.append("\n").toString();
    }

    private static int getMaxItemLength(List<String> colData) {
        int maxLength = 0;
        for (String aColData : colData)
            maxLength = Math.max(aColData.length(), maxLength);
        return maxLength;
    }

    private static int getMaxColumns(String[] header, String[][] data) {
        int maxColumns = 0;
        for (String[] aData : data)
            maxColumns = Math.max(aData.length, maxColumns);
        maxColumns = Math.max(header.length, maxColumns);
        return maxColumns;
    }

    private static List<Integer> getMaxColLengths(int colCount, String[] header, String[][] data) {
        List<Integer> colMaxLenList = new ArrayList<Integer>(colCount);
        List<String> colData = null;
        int maxLength;
        for (int i = 0; i < colCount; i++) {
            colData = new ArrayList<String>();
            if (header != null && i < header.length)
                colData.add(header[i]);
            for (String[] aData : data) {
                if (i < aData.length)
                    colData.add(aData[i]);
                else
                    colData.add("");
            }
            maxLength = getMaxItemLength(colData);
            colMaxLenList.add(maxLength);
        }
        return colMaxLenList;
    }

    private static String[] getHeaders(Column[] headerObjs) {
        String[] header = new String[0];
        if (headerObjs != null && headerObjs.length > 0) {
            header = new String[headerObjs.length];
            for (int i = 0; i < headerObjs.length; i++)
                header[i] = headerObjs[i].header;
        }
        return header;
    }
}