package commands;

import net.dv8tion.jda.entities.*;
import org.hurst.bot.*;
import org.hurst.discord.command.CommandRepository.*;
import org.hurst.discord.member.*;
import org.hurst.util.*;

import java.util.*;
import java.util.concurrent.*;

/**
 * Created by tyler on 12/15/2016.
 */
public class PurgeCommand extends Command {
    private final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

    public PurgeCommand() {
        super("purge", "Purge a specified amount of previous channel messages.", true);
    }

    @Override
    public void execute(DiscordBot bot, Member member, MessageChannel channel, Message message, String[] arguments) {
        try {
            Integer amount = Integer.parseInt(combineArguments(arguments));
            int count = 0;
            List<Message> messages;
            while ((messages = channel.getHistory().retrieveAll()).size() != 0) {
                for (Message msg : messages) {
                    if (channel.deleteMessageById(msg.getId()))
                        count++;
                }
            }

            Message response = channel.sendMessage("Okay, I've deleted " + BotUtil.bold(count + "") + " messages.");
            service.schedule(response::deleteMessage, 5, TimeUnit.SECONDS);
        } catch (Exception e) {
            channel.sendMessage(member.getUser().getAsMention() + " you need to add a simple number value of the amount of messages you wish to purge.");
            e.printStackTrace();
        }
    }
}
