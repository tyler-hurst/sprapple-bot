package org.hurst.core.task;

import com.google.common.collect.*;
import org.hurst.bot.*;
import org.hurst.util.*;

import java.util.*;
import java.util.concurrent.*;

/**
 * Created by tyler on 12/16/2016.
 */
public class TaskManager {
    private final List<Task> tasks = Lists.newLinkedList();
    private final ScheduledExecutorService service = Executors.newScheduledThreadPool(2);
    private final DiscordBot bot;

    public TaskManager(DiscordBot bot) {
        this.bot = bot;
        List<Task> taskFiles = FileUtil.collect("./src/org/hurst/task/impl/", Task.class);
        taskFiles.forEach(this::add);
    }

    public void add(Task task) {
        task.setBot(bot);
        service.scheduleAtFixedRate(task, task.getInitialDelay(), task.getPeriod(), TimeUnit.MILLISECONDS);
    }
}
