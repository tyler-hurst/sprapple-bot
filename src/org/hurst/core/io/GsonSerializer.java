package org.hurst.core.io;

import com.google.common.base.*;
import com.google.gson.*;
import org.hurst.util.*;

import java.io.*;
import java.lang.reflect.*;
import java.nio.file.*;
import java.text.*;

/**
 * @author Tyler Hurst
 */
public class GsonSerializer {
    private final Gson GSON;
    private final GsonBuilder builder = new GsonBuilder();
    private final Type type;

    public GsonSerializer(Type type) {
        this.type = type;
        builder.enableComplexMapKeySerialization();
        builder.serializeNulls();
        builder.setDateFormat(DateFormat.LONG);
        builder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY);
        builder.setPrettyPrinting();
        builder.setVersion(1.0);
        GSON = builder.create();
    }

    public Object deserialize(String dir) {
        try (Reader reader = Files.newBufferedReader(Paths.get(dir))) {
            return GSON.fromJson(reader, type);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void serialize(Object src, String dir) {
        try {
            try (Writer writer = Files.newBufferedWriter(Preconditions.checkNotNull(FileUtil.getPathOrCreate(dir)))) {
                writer.write(GSON.toJson(src, type));
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}