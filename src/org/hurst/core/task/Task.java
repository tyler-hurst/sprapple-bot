package org.hurst.core.task;

import org.hurst.bot.*;

import java.util.concurrent.*;

/**
 * Created by tyler on 12/16/2016.
 */
public abstract class Task implements Runnable {
    private final long delay;
    private final long initialDelay;
    protected DiscordBot bot;

    public Task(long delay, TimeUnit unit) {
        this(unit.toMillis(delay), unit.toMillis(delay));
    }

    public Task(long delay, long initialDelay, TimeUnit unit) {
        this(unit.toMillis(delay), unit.toMillis(initialDelay));
    }

    public Task(long delay, long initialDelay) {
        this.delay = delay;
        this.initialDelay = initialDelay;
    }

    @Override
    public abstract void run();

    public long getInitialDelay() {
        return initialDelay;
    }

    public long getPeriod() {
        return delay;
    }

    public void setBot(DiscordBot bot) {
        this.bot = bot;
    }
}
