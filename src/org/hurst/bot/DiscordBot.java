package org.hurst.bot;

import com.google.common.collect.*;
import net.dv8tion.jda.*;
import net.dv8tion.jda.entities.*;
import org.hurst.bot.listener.*;
import org.hurst.core.io.*;
import org.hurst.core.task.*;
import org.hurst.discord.member.*;
import org.hurst.util.*;

import java.util.*;

import static java.util.Objects.*;

/**
 * Created by tyler on 12/14/2016.
 */
public class DiscordBot {
    private static final Logger logger = Logger.createDefaultLogger();
    private final GsonMemberSerializer memberSerializer = new GsonMemberSerializer();
    private final Map<String, Member> members = Maps.newHashMap();
    private final TaskManager taskManager = new TaskManager(this);
    private final GsonSerializer configSerializer = new GsonSerializer(Config.class);
    private Config config = new Config();
    private JDA jda;
    private Guild server;

    public DiscordBot() throws Exception {
        logger.info("Loading Sprapple's discord bot..");
        SystemUtil.addUncaughtExceptionAction(() -> System.exit(1));
        SystemUtil.createShutdownHook(this::shutdown);

        logger.info("Verifying configurations..");
        config = (Config) configSerializer.deserialize("./resources/config.json");
        if (config == null) {
            logger.error("Config file was corrupted? Creating new default instance..");
            config = new Config();
            configSerializer.serialize(config, "./resources/config.json");
        }
    }

    public void initialize() {
        logger.info("Binding listeners..");
        List<BotListener> listeners = FileUtil.collect("./src/org/hurst/bot/listener/impl/", BotListener.class);
        listeners.forEach(listener -> listener.bind(this, server));
        logger.info("Bound {} listeners.", listeners.size());

        logger.info("Fetching user data..");
        server.getUsers().forEach(user -> {
            Member member = getMember(user);
            members.putIfAbsent(user.getDiscriminator(), member);
        });
        logger.info("Loaded {} online users.", members.size());

        logger.info("Ready for operation.");
    }

    public void login() {
        try {
            logger.info("Logging into bot..");
            jda = new JDABuilder().setBotToken(config.getApiKey()).setBulkDeleteSplittingEnabled(true).buildBlocking();
            server = jda.getGuildById("246057632312328193");
        } catch (Exception e) {
            e.printStackTrace();
        }
        requireNonNull(jda, "There was a problem logging into the bot, please verify your credentials.");
        requireNonNull(server,
                "Invalid server id. " + (jda.getGuilds().size() == 0 ? "Please add this bot to a server before launching." : "Please reconfigure to one of " +
                        "" + "" + "the valid servers: " + jda.getGuilds()));
    }

    public void sendMessage(String channel, String message) {
        server.getTextChannels().stream().filter(c -> c.getName().equalsIgnoreCase(channel)).findAny().ifPresent(c -> c.sendMessage(message));
    }

    public void sendMessage(String message) {
        sendMessage(config.getDesignatedBotSpamChannel(), message);
    }

    public Member getMember(User user) {
        Member member = members.getOrDefault(user.getDiscriminator(), memberSerializer.deserialize(user));
        if (member == null) {
            logger.error("This shouldn't happen.");
            return new Member(user).setBot(this);
        }
        return member.setBot(this);
    }

    public void saveConfiguration() {
        logger.info("Saving configurations..");
        configSerializer.serialize(config, "./resources/config.json");
    }

    public void shutdown() {
        sendMessage("Going offline for now, bye!");
        saveConfiguration();
        logger.info("Saving member data..");
        members.forEach((k, v) -> memberSerializer.serialize(v));
        logger.info("Shutting down bot api..");
        jda.shutdown();
        try {
            //We need to try to give the system time to exit properly.
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public TaskManager getTaskManager() {
        return taskManager;
    }

    public GsonMemberSerializer getMemberSerializer() {
        return memberSerializer;
    }

    public Config getConfig() {
        return config;
    }

    public Guild getServer() {
        return server;
    }

    public JDA getJDA() {
        return jda;
    }

    // sprapple-bot#1083
    //MjU4ODEwNjQzODE2MjUxMzky.CzOrtg.pyoJwRKkdujRRoAlkkE-XAOxkb8
}
