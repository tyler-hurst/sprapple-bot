package org.hurst.util;

import java.io.*;
import java.nio.file.*;
import java.util.*;

/**
 * Created by tyler on 12/14/2016.
 */
public class FileUtil {
    public static <E> LinkedList<E> collect(String path, Class<?> superclass) {
        LinkedList<E> list = new LinkedList<E>();
        try {
            Files.walk(Paths.get(path)).filter(Files::isRegularFile).iterator().forEachRemaining(p -> {
                Class<?> clazz = getClass(p.toFile());
                if (clazz == null || !superclass.isAssignableFrom(clazz))
                    return;
                E var = (E) instance(clazz);
                if (var != null)
                    list.add((E) var);
            });
        } catch (IOException e) {
            // logger.error("There was a problem looping through {}", path, e);
        }
        return list;
    }

    public static Class<?> getClass(File file) {
        try {
            String name = file.getName();
            if (name.endsWith(".java")) {
                return Class.forName(file.getPath().replace(File.separatorChar, '.').replace("..src.", "").replace(".java", ""));
            } else if (name.endsWith(".groovy")) {
                return Class.forName(file.getPath().replace(File.separatorChar, '.').replace("..src.", "").replace(".groovy", ""));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T instance(Class<T> cl) {
        try {
            return cl.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            // e.printStackTrace();
        }
        return null;
    }

    public static Path getPath(String directory) {
        return Paths.get(directory).normalize();
    }

    public static Path getPathOrCreate(String directory) {
        return create(getPath(directory));
    }

    public static List<String> list(String file) {
        try {
            return Files.readAllLines(Paths.get(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Path create(Path target) {
        try {
            if (Files.exists(target))
                return target;
            if (!Files.exists(target.getParent()))
                Files.createDirectories(target.getParent());
            return Files.createFile(target);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
