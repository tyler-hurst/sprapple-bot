package org.hurst.util;

/**
 * Created by tyler on 8/1/2016.
 */
public class SystemUtil {
    private static final Logger logger = Logger.createDefaultLogger();

    public static void addUncaughtExceptionAction(Runnable command) {
        Thread.currentThread().setUncaughtExceptionHandler((t, e) -> {
            logger.error("Uncaught exception in thread: " + t.getName(), e);
            command.run();
        });
    }

    public static void createShutdownHook(Runnable hook) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                hook.run();
            }
        });
    }
}