package commands;

import net.dv8tion.jda.entities.*;
import org.hurst.bot.*;
import org.hurst.discord.command.*;
import org.hurst.discord.command.CommandRepository.*;
import org.hurst.discord.member.*;

/**
 * Created by tyler on 12/15/2016.
 */
public class HelpCommand extends Command {
    private static final CommandRepository repository = new CommandRepository();

    public HelpCommand() {
        super("help", "Displays this list of commands");
    }

    @Override
    public void execute(DiscordBot bot, Member member, MessageChannel channel, Message message, String[] arguments) {
        StringBuilder builder = new StringBuilder();
        repository.forEach((s, command) -> builder.append(command).append("\n"));
        channel.sendMessage("Here's a list of my current commands! \n " + builder.toString());
    }
}
