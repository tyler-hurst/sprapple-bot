package org.hurst.bot.listener;

import net.dv8tion.jda.*;
import net.dv8tion.jda.entities.*;
import net.dv8tion.jda.hooks.*;
import org.hurst.bot.*;
import org.hurst.util.*;

/**
 * Created by tyler on 12/14/2016.
 */
public abstract class BotListener extends ListenerAdapter {
    private static final Logger logger = Logger.createDefaultLogger();
    protected Guild guild;
    protected JDA jda;
    protected DiscordBot bot;

    public BotListener bind(DiscordBot bot, Guild guild) {
        this.bot = bot;
        logger.info("Adding event listener: {}", this.getClass().getSimpleName());
        this.guild = guild;
        this.jda = guild.getJDA();
        this.guild.getJDA().addEventListener(this);
        return this;
    }

}
