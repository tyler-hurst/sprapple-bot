package org.hurst.bot.listener.impl;

import net.dv8tion.jda.events.guild.member.*;
import org.hurst.bot.listener.*;

/**
 * Created by tyler on 12/18/2016.
 */
public class WelcomeUserListener extends BotListener {
    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        super.onGuildMemberJoin(event);
        bot.sendMessage(bot.getConfig().getMainChannel(), bot.getConfig().getWelcomeMessage().replace("%NAME%", event.getUser().getAsMention()));
    }

    @Override
    public void onGuildMemberLeave(GuildMemberLeaveEvent event) {
        super.onGuildMemberLeave(event);
        bot.sendMessage(bot.getConfig().getMainChannel(), bot.getConfig().getFarewellMessage().replace("%NAME%", event.getOldNick()));
    }
}
