package org.hurst.core.task.impl;

import org.hurst.core.task.*;
import org.hurst.util.*;
import twitter4j.*;

import java.util.*;
import java.util.concurrent.*;

/**
 * Created by tyler on 12/16/2016.
 */
public class ResetNotificationTask extends Task {
    private final TwitterReader reader = new TwitterReader(Arrays.asList("jagexclock"));

    public ResetNotificationTask() {
        super(1, TimeUnit.MINUTES);
        reader.setStatusListener(STATUS_LISTENER);
    }

    @Override
    public void run() {
        String time = TimeUtil.getGameTime();
        switch (time) {
            case "18:00":
                bot.sendMessage("It is now resets, relog for your dailies.");
                break;
            case "17:55":
                bot.sendMessage("Five minutes to resets!");
                break;
        }
    }

    private final StatusListener STATUS_LISTENER = new StatusListener() {
        @Override
        public void onStatus(Status status) {
            if (status.getText().startsWith("RT "))
                return;
            if (status.getText().contains("Wilderness Warbands"))
                return;

            if (status.getText().toLowerCase().startsWith("the voice of seren is now active in the")) {
                String voice1 = status.getText().split("The Voice of Seren is now active in the ")[1].split(" ")[0];
                String voice2 = status.getText().split(" and ")[1].split(" ")[0];
                bot.sendMessage("\nThis hour's VoS is: " + BotUtil.bold(voice1) + " and " + BotUtil.bold(voice2));
            } else
                bot.sendMessage(status.getText().replace("@", "https://twitter.com/"));
        }

        @Override
        public void onException(Exception arg0) {
        }

        @Override
        public void onDeletionNotice(StatusDeletionNotice arg0) {
        }

        @Override
        public void onScrubGeo(long arg0, long arg1) {
        }

        @Override
        public void onStallWarning(StallWarning stallWarning) {
        }

        @Override
        public void onTrackLimitationNotice(int arg0) {
        }

    };
}
