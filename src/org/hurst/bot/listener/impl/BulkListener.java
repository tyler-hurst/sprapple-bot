package org.hurst.bot.listener.impl;

import net.dv8tion.jda.events.*;
import net.dv8tion.jda.events.audio.*;
import net.dv8tion.jda.events.channel.priv.*;
import net.dv8tion.jda.events.channel.text.*;
import net.dv8tion.jda.events.channel.voice.*;
import net.dv8tion.jda.events.guild.*;
import net.dv8tion.jda.events.guild.member.*;
import net.dv8tion.jda.events.guild.role.*;
import net.dv8tion.jda.events.message.*;
import net.dv8tion.jda.events.message.guild.*;
import net.dv8tion.jda.events.message.priv.*;
import net.dv8tion.jda.events.user.*;
import net.dv8tion.jda.events.voice.*;
import org.hurst.bot.listener.*;

/**
 * Created by tyler on 12/15/2016.
 */
public class BulkListener extends BotListener {
    @Override
    public void onResume(ResumedEvent event) {
        super.onResume(event);
    }

    @Override
    public void onReconnect(ReconnectedEvent event) {
        super.onReconnect(event);
    }

    @Override
    public void onDisconnect(DisconnectEvent event) {
        super.onDisconnect(event);
    }

    @Override
    public void onShutdown(ShutdownEvent event) {
        super.onShutdown(event);
        bot.sendMessage("Sorry guys, going offline!");
    }

    @Override
    public void onStatusChange(StatusChangeEvent event) {
        super.onStatusChange(event);

    }

    @Override
    public void onUserNameUpdate(UserNameUpdateEvent event) {
        super.onUserNameUpdate(event);
        bot.sendMessage(event.getPreviousUsername() + " has changed their name to " + event.getUser().getUsername());
    }

    @Override
    public void onUserAvatarUpdate(UserAvatarUpdateEvent event) {
        super.onUserAvatarUpdate(event);
    }

    @Override
    public void onUserOnlineStatusUpdate(UserOnlineStatusUpdateEvent event) {
        super.onUserOnlineStatusUpdate(event);
    }

    @Override
    public void onUserGameUpdate(UserGameUpdateEvent event) {
        super.onUserGameUpdate(event);
    }

    @Override
    public void onUserTyping(UserTypingEvent event) {
        super.onUserTyping(event);
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        super.onGuildMessageReceived(event);
    }

    @Override
    public void onGuildMessageUpdate(GuildMessageUpdateEvent event) {
        super.onGuildMessageUpdate(event);
    }

    @Override
    public void onGuildMessageDelete(GuildMessageDeleteEvent event) {
        super.onGuildMessageDelete(event);
    }

    @Override
    public void onGuildMessageEmbed(GuildMessageEmbedEvent event) {
        super.onGuildMessageEmbed(event);
    }

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event) {
        super.onPrivateMessageReceived(event);
    }

    @Override
    public void onPrivateMessageUpdate(PrivateMessageUpdateEvent event) {
        super.onPrivateMessageUpdate(event);
    }

    @Override
    public void onPrivateMessageDelete(PrivateMessageDeleteEvent event) {
        super.onPrivateMessageDelete(event);
    }

    @Override
    public void onPrivateMessageEmbed(PrivateMessageEmbedEvent event) {
        super.onPrivateMessageEmbed(event);
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        super.onMessageReceived(event);
    }

    @Override
    public void onMessageUpdate(MessageUpdateEvent event) {
        super.onMessageUpdate(event);
    }

    @Override
    public void onMessageDelete(MessageDeleteEvent event) {
        super.onMessageDelete(event);
    }



    @Override
    public void onInviteReceived(InviteReceivedEvent event) {
        super.onInviteReceived(event);
    }

    @Override
    public void onTextChannelDelete(TextChannelDeleteEvent event) {
        super.onTextChannelDelete(event);
        bot.sendMessage("Deleted channel: " + event.getChannel().getName());
    }

    @Override
    public void onTextChannelUpdateName(TextChannelUpdateNameEvent event) {
        super.onTextChannelUpdateName(event);
        event.getChannel().sendMessage("Channel name update:  " + event.getOldName() + " -> " + event.getChannel().getName());
    }

    @Override
    public void onTextChannelUpdateTopic(TextChannelUpdateTopicEvent event) {
        super.onTextChannelUpdateTopic(event);
        event.getChannel().sendMessage("Channel topic update:  " + event.getOldTopic() + " -> " + event.getChannel().getTopic());
    }




    @Override
    public void onTextChannelCreate(TextChannelCreateEvent event) {
        super.onTextChannelCreate(event);
        bot.sendMessage("A new text channel has been created: " + event.getChannel().getName());
    }

    @Override
    public void onVoiceChannelDelete(VoiceChannelDeleteEvent event) {
        super.onVoiceChannelDelete(event);
        bot.sendMessage("Deleted voice channel: " + event.getChannel().getName());
    }

    @Override
    public void onVoiceChannelUpdateName(VoiceChannelUpdateNameEvent event) {
        super.onVoiceChannelUpdateName(event);
        bot.sendMessage("Updated voice channel name: " + event.getOldName() + " -> " + event.getChannel().getName());

    }



    @Override
    public void onVoiceChannelUpdateUserLimit(VoiceChannelUpdateUserLimitEvent event) {
        super.onVoiceChannelUpdateUserLimit(event);
        bot.sendMessage("The voice channel user limit for channel: " + event.getChannel().getName() + " has been updated from  " + event.getOldUserLimit()
                + " to " + event.getChannel().getUserLimit());
    }



    @Override
    public void onVoiceChannelUpdatePermissions(VoiceChannelUpdatePermissionsEvent event) {
        super.onVoiceChannelUpdatePermissions(event);
    }

    @Override
    public void onVoiceChannelCreate(VoiceChannelCreateEvent event) {
        super.onVoiceChannelCreate(event);
    }

    @Override
    public void onPrivateChannelCreate(PrivateChannelCreateEvent event) {
        super.onPrivateChannelCreate(event);
    }

    @Override
    public void onGuildJoin(GuildJoinEvent event) {
        super.onGuildJoin(event);
    }

    @Override
    public void onUnavailGuildJoined(UnavailableGuildJoinedEvent event) {
        super.onUnavailGuildJoined(event);
    }

    @Override
    public void onGuildUpdate(GuildUpdateEvent event) {
        super.onGuildUpdate(event);
    }







    @Override
    public void onGuildMemberBan(GuildMemberBanEvent event) {
        super.onGuildMemberBan(event);
        bot.sendMessage(bot.getConfig().getMainChannel(), event.getUser().getAsMention() + " has been banned from the server.");

    }

    @Override
    public void onGuildMemberUnban(GuildMemberUnbanEvent event) {
        super.onGuildMemberUnban(event);
        bot.sendMessage(bot.getConfig().getMainChannel(), event.getUser().getAsMention() + " has been unbanned from the server.");
    }

    @Override
    public void onGuildMemberRoleAdd(GuildMemberRoleAddEvent event) {
        super.onGuildMemberRoleAdd(event);
    }

    @Override
    public void onGuildMemberRoleRemove(GuildMemberRoleRemoveEvent event) {
        super.onGuildMemberRoleRemove(event);
    }

    @Override
    public void onGuildMemberNickChange(GuildMemberNickChangeEvent event) {
        super.onGuildMemberNickChange(event);
        bot.sendMessage(event.getPrevNick() + " has changed their nickname to " + event.getNewNick());

    }

    @Override
    public void onGuildRoleCreate(GuildRoleCreateEvent event) {
        super.onGuildRoleCreate(event);
    }

    @Override
    public void onGuildRoleDelete(GuildRoleDeleteEvent event) {
        super.onGuildRoleDelete(event);
    }

    @Override
    public void onGuildRoleUpdate(GuildRoleUpdateEvent event) {
        super.onGuildRoleUpdate(event);
    }

    @Override
    public void onGuildRoleUpdateName(GuildRoleUpdateNameEvent event) {
        super.onGuildRoleUpdateName(event);
    }

    @Override
    public void onGuildRoleUpdateColor(GuildRoleUpdateColorEvent event) {
        super.onGuildRoleUpdateColor(event);
    }

    @Override
    public void onGuildRoleUpdatePosition(GuildRoleUpdatePositionEvent event) {
        super.onGuildRoleUpdatePosition(event);
    }

    @Override
    public void onGuildRoleUpdatePermission(GuildRoleUpdatePermissionEvent event) {
        super.onGuildRoleUpdatePermission(event);
    }

    @Override
    public void onGuildRoleUpdateGrouped(GuildRoleUpdateGroupedEvent event) {
        super.onGuildRoleUpdateGrouped(event);
    }

    @Override
    public void onVoiceSelfMute(VoiceSelfMuteEvent event) {
        super.onVoiceSelfMute(event);
    }

    @Override
    public void onVoiceSelfDeaf(VoiceSelfDeafEvent event) {
        super.onVoiceSelfDeaf(event);
    }

    @Override
    public void onVoiceServerMute(VoiceServerMuteEvent event) {
        super.onVoiceServerMute(event);
    }

    @Override
    public void onVoiceServerDeaf(VoiceServerDeafEvent event) {
        super.onVoiceServerDeaf(event);
    }

    @Override
    public void onVoiceMute(VoiceMuteEvent event) {
        super.onVoiceMute(event);
    }

    @Override
    public void onVoiceDeaf(VoiceDeafEvent event) {
        super.onVoiceDeaf(event);
    }

    @Override
    public void onVoiceJoin(VoiceJoinEvent event) {
        super.onVoiceJoin(event);
    }

    @Override
    public void onVoiceLeave(VoiceLeaveEvent event) {
        super.onVoiceLeave(event);
    }

    @Override
    public void onAudioConnect(AudioConnectEvent event) {
        super.onAudioConnect(event);
    }

    @Override
    public void onAudioDisconnect(AudioDisconnectEvent event) {
        super.onAudioDisconnect(event);
    }

    @Override
    public void onAudioUnableToConnect(AudioUnableToConnectEvent event) {
        super.onAudioUnableToConnect(event);
        bot.sendMessage("Hey! I was unable to join " + event.getChannel().getName() + "'s voice channel.");
    }

    @Override
    public void onAudioTimeout(AudioTimeoutEvent event) {
        super.onAudioTimeout(event);
    }

    @Override
    public void onAudioRegionChange(AudioRegionChangeEvent event) {
        super.onAudioRegionChange(event);
    }

    @Override
    public void onGenericUserEvent(GenericUserEvent event) {
        super.onGenericUserEvent(event);
    }

    @Override
    public void onGenericMessage(GenericMessageEvent event) {
        super.onGenericMessage(event);
    }

    @Override
    public void onGenericGuildMessage(GenericGuildMessageEvent event) {
        super.onGenericGuildMessage(event);
    }

    @Override
    public void onGenericPrivateMessage(GenericPrivateMessageEvent event) {
        super.onGenericPrivateMessage(event);
    }

    @Override
    public void onGenericTextChannel(GenericTextChannelEvent event) {
        super.onGenericTextChannel(event);
    }

    @Override
    public void onGenericTextChannelUpdate(GenericTextChannelUpdateEvent event) {
        super.onGenericTextChannelUpdate(event);
    }

    @Override
    public void onGenericVoiceChannel(GenericVoiceChannelEvent event) {
        super.onGenericVoiceChannel(event);
    }

    @Override
    public void onGenericVoiceChannelUpdate(GenericVoiceChannelUpdateEvent event) {
        super.onGenericVoiceChannelUpdate(event);
    }

    @Override
    public void onGenericGuildMember(GenericGuildMemberEvent event) {
        super.onGenericGuildMember(event);
    }

    @Override
    public void onGenericGuild(GenericGuildEvent event) {
        super.onGenericGuild(event);
    }

    @Override
    public void onGenericVoice(GenericVoiceEvent event) {
        super.onGenericVoice(event);
    }

    @Override
    public void onGenericAudio(GenericAudioEvent event) {
        super.onGenericAudio(event);
    }

    @Override
    public void onGenericGuildRoleUpdate(GenericGuildRoleUpdateEvent event) {
        super.onGenericGuildRoleUpdate(event);
    }

    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
    }
}
