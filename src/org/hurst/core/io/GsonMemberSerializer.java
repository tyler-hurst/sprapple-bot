package org.hurst.core.io;

import net.dv8tion.jda.entities.*;
import org.hurst.discord.member.*;

import java.nio.file.*;

/**
 * Created by tyler on 12/15/2016.
 */
public class GsonMemberSerializer {
    private static final GsonSerializer serializer = new GsonSerializer(Member.class);

    public Member deserialize(User user) {
        Path path = Paths.get("./resources/members/" + user.getDiscriminator() + ".json");
        if (!Files.exists(path))
            return new Member(user);
        Member member = (Member) serializer.deserialize(path.toString());
        member.setUser(user);
        return member;
    }

    public void serialize(Member user) {
        serializer.serialize(user, "./resources/members/" + user.getDiscriminator() + ".json");
    }
}
