package org.hurst.core.task.impl;

import org.hurst.core.task.*;
import org.hurst.util.*;

import java.util.concurrent.*;

/**
 * Created by tyler on 12/16/2016.
 */
public class CreateParadyGameStatusTask extends Task {
    public CreateParadyGameStatusTask() {
        super(1, TimeUnit.MINUTES);
    }

    @Override
    public void run() {
        bot.getJDA().getAccountManager().setStreaming(RandomUtil.get(bot.getConfig().getParodyGameStatusList()), bot.getConfig().getDefaultTwitchChannel());
        bot.getJDA().getAccountManager().update();
    }

}
