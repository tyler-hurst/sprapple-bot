package org.hurst.util.ascii_table;

import java.util.function.*;

public class Column {
    public final String header;
    public final Alignment headerAlignment;
    public final Alignment dataAlignment;

    public Column(String headerName) {
        this(headerName, Alignment.HEADER_DEFAULT, Alignment.DATA_DEFAULT);
    }

    public Column(String header, Alignment headerAlignment, Alignment dataAlignment) {
        this.header = header;
        this.headerAlignment = headerAlignment;
        this.dataAlignment = dataAlignment;
    }

    public <T> Data<T> with(Function<T, String> getter) {
        return new Data<T>(this, getter);
    }

    public static class Data<T> {
        public final Column column;
        public final Function<T, String> getter;

        private Data(Column column, Function<T, String> getter) {
            this.column = column;
            this.getter = getter;
        }
    }
}