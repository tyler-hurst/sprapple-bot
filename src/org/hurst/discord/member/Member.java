package org.hurst.discord.member;

import com.google.common.base.*;
import com.google.common.collect.*;
import net.dv8tion.jda.entities.*;
import org.hurst.bot.*;
import org.hurst.util.*;

import java.util.*;
import java.util.concurrent.*;

/**
 * Created by tyler on 12/15/2016.
 */
public class Member {
    private String username;
    private transient User user;
    private final boolean isBot;
    private final String discriminator;
    private Map<Restrictions, Long> restrictions = Maps.newHashMap();
    private transient DiscordBot bot;

    public Member(User user) {
        this.user = user;
        this.username = user.getUsername();
        this.isBot = user.isBot();
        this.discriminator = user.getDiscriminator();
    }

    public boolean hasRole(String roleName) {
        return bot.getServer().getRolesForUser(user).stream().anyMatch(role -> role.getName().equalsIgnoreCase(roleName));
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void addRestriction(Restrictions restriction) {
        restrictions.putIfAbsent(restriction, TimeUnit.HOURS.toMillis(bot.getConfig().getDefaultHourRestrictionPunishment()));
    }

    public boolean restricted(Restrictions restriction) {
        return restrictions.getOrDefault(restriction, 0L) < TimeUtil.currentTimeMillis();
    }

    public Member setBot(DiscordBot bot) {
        this.bot = bot;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("username", username).toString();
    }

    public String getUsername() {
        return username;
    }

    public String getDiscriminator() {
        return discriminator;
    }

    public enum Restrictions {
        CANNOT_USE_COMMANDS, DELAYED_COMMANDS;
    }
}
