import org.hurst.bot.*;

/**
 * Created by tyler on 12/17/2016.
 */
public class RunApplication {
    public static void main(String[] args) {
        try {
            DiscordBot bot = new DiscordBot();
            bot.login();
            bot.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}