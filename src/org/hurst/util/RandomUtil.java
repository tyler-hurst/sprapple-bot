package org.hurst.util;

import java.util.*;

/**
 * Created by tyler on 12/14/2016.
 */
public class RandomUtil {
    public static int get(int maxValue) {
        return (int) (Math.random() * (maxValue + 1));
    }

    public static int range(int min, int max) {
        final int n = Math.abs(max - min);
        return Math.min(min, max) + (n == 0 ? 0 : get(n));
    }

    public static <T> T get(T... objs) {
        return objs[get(objs.length - 1)];
    }

    public static <V> V get(List<V> list) {
        return list.get(get(list.size() - 1));
    }
}
