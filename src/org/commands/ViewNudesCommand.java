package commands;

import com.google.common.reflect.*;
import net.dv8tion.jda.entities.*;
import org.hurst.bot.*;
import org.hurst.core.io.*;
import org.hurst.discord.command.CommandRepository.*;
import org.hurst.discord.member.*;
import org.hurst.util.*;

import java.util.*;

/**
 * Created by tyler on 12/15/2016.
 */
public class ViewNudesCommand extends Command {
    private static final GsonSerializer serializer = new GsonSerializer(new TypeToken<List<String>>() {
    }.getType());

    public ViewNudesCommand() {
        super("nudes", "Sends you.. some.. interesting pictures. (ง ͠° ͟ل͜ ͡°)ง ");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void execute(DiscordBot bot, Member member, MessageChannel channel, Message message, String[] arguments) {
        List<String> nudes = (List<String>) serializer.deserialize("./resources/nudes-repository");
        channel.sendMessage(member.getUser().getAsMention() + " (ง ͠° ͟ل͜ ͡°)ง " + RandomUtil.get(nudes));
    }
}
