package org.hurst.discord.command;

import net.dv8tion.jda.entities.*;
import org.hurst.bot.*;
import org.hurst.discord.command.CommandRepository.*;
import org.hurst.discord.member.*;
import org.hurst.util.*;

import java.util.*;

/**
 * Created by tyler on 12/15/2016.
 */
public class CommandRepository extends HashMap<String, Command> {
    public CommandRepository() {
        List<Command> commands = FileUtil.collect("./src/commands/", Command.class);
        commands.forEach(command -> put(command.id, command));
    }

    public static abstract class Command {
        protected final String id;
        protected final String description;
        protected boolean managerialCommand;

        public Command(String id, String description, boolean managerialCommand) {
            this.id = id;
            this.description = description;
            this.managerialCommand = managerialCommand;
        }

        public Command(String id, String description) {
            this(id, description, false);
        }

        public boolean isManagerialCommand() {
            return managerialCommand;
        }

        public abstract void execute(DiscordBot bot, Member member, MessageChannel channel, Message message, String[] arguments);

        public Member getTaggedTarget(DiscordBot bot, Member member, MessageChannel channel, Message message) {
            if (message.getMentionedUsers().size() != 1) {
                channel.sendMessage(member.getUser().getAsMention() + " you need to @tag the specific user you wish to cmdban.");
                return null;
            }
            return bot.getMember(message.getMentionedUsers().get(0));
        }

        public String combineArguments(String[] args, String additionToEachArg) {
            String url = "";
            for (int i = 0; i < args.length; i++) {
                url += args[i] + additionToEachArg;
            }
            return url;
        }

        public String combineArguments(String[] args) {
            return combineArguments(args, "");
        }

        @Override
        public String toString() {
            return BotUtil.orange("Command: " + id + "\nDescription: " + description + (managerialCommand ? "\nmanagerialCommand: " + managerialCommand :
                    ""));
        }

    }

}
