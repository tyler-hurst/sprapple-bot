package org.hurst.util;

import twitter4j.*;
import twitter4j.conf.*;

import java.util.*;

/**
 * Created by tyler on 6/22/2016.
 */
public class TwitterReader {
    private final List<String> accounts = new ArrayList<>();
    private final Twitter twitter = new TwitterFactory(new ConfigurationBuilder().setOAuthConsumerKey("JCnuWFXaaxa5rd6s4hKbKe7lp").setOAuthConsumerSecret(
            "lJTlDsmgmoYhGUc9AhnRBovJGJVRgEPjwut44jYC5PF6ehoGWg").setOAuthAccessToken("898947798-hNgdntXmRzJ0sJmPbep5vd15DwN07s05czZVy79P")
            .setOAuthAccessTokenSecret(
            "rVbe7WE6qvbpfl3Tv7pvsITWfe8sUpVfHfMSXtox3nnh4").build()).getInstance();
    private final TwitterStream twitterStream = new TwitterStreamFactory(twitter.getConfiguration()).getInstance();
    private StatusListener listener;

    public TwitterReader(List<String> accountList) {
        this.accounts.addAll(accountList);
        long[] names = new long[0];

        for (String string : accounts) {
            try {
                long name = twitter.showUser(string).getId();
                names = addElement(names, name);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        twitterStream.addListener(listener);
        FilterQuery fq = new FilterQuery(names);
        twitterStream.filter(fq);
    }

    private long[] addElement(long[] ar, long v) {
        long[] result = Arrays.copyOf(ar, ar.length + 1);
        result[ar.length] = v;
        return result;
    }

    public void setStatusListener(StatusListener listener) {
        twitterStream.clearListeners();
        this.listener = listener;
        if (listener != null)
            twitterStream.addListener(this.listener);
    }
}