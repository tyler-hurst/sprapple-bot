package commands;

import net.dv8tion.jda.entities.*;
import org.hurst.bot.*;
import org.hurst.discord.command.CommandRepository.*;
import org.hurst.discord.member.*;
import org.hurst.util.*;

import java.time.temporal.*;
import java.util.*;

/**
 * Created by tyler on 12/15/2016.
 */
public class PingCommand extends Command {
    public PingCommand() {
        super("ping", "Check the response time of this service");
    }

    @Override
    public void execute(DiscordBot bot, Member member, MessageChannel channel, Message message, String[] arguments) {
        long ping = message.getTime().toInstant().until(new Date().toInstant(), ChronoUnit.MILLIS);
        channel.sendMessage(member.getUser().getAsMention() + ", pong! Time taken: " + TimeUtil.prettyTimeDuration(ping) + ".");
    }
}
