package org.hurst.util;

import com.google.common.collect.*;

import java.io.*;
import java.net.*;
import java.nio.charset.*;
import java.util.*;

/**
 * Created by tyler on 11/18/2016.
 */
public class WebUtil {
    public static LinkedList<String> readAllLines(String url) {
        LinkedList<String> list = Lists.newLinkedList();
        try {
            URLConnection conn = new URL(url).openConnection();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                reader.lines().forEach(list::add);
            }
            return list;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}