package org.hurst.util;

import java.text.*;
import java.util.*;

import static org.hurst.util.Logger.LogLevel.*;

/**
 * @author Tyler Hurst
 */
public class Logger {
    private SimpleDateFormat dateFormat;
    private boolean showDateTime = true;
    private boolean showThreadName = true;
    private boolean hyperlink = true;
    private static final Logger DEFAULT_LOGGER = new Logger().setDateFormat("HH:mm:ss").setShowThreadName(false);

    public static Logger createDefaultLogger() {
        return DEFAULT_LOGGER;
    }

    public void info(String message) {
        log(message, INFO);
    }

    public void info(String message, Object... var) {
        if (message.contains("{}"))
            log(message.replace("{}", var[0].toString()), INFO);
        else {
            for (int i = 0; i < var.length; i++)
                log(message.replace("{" + i + "}", "" + var[i]), INFO);
        }
    }

    public void debug(String message) {
        log(message, DEBUG);
    }

    public void debug(String message, Object... var) {
        if (message.contains("{}"))
            log(message.replace("{}", var[0].toString()), DEBUG);
        else {
            for (int i = 0; i < var.length; i++)
                log(message.replace("{" + i + "}", "" + var[i]), DEBUG);
        }
    }

    public void warn(String message) {
        log(message, WARNING);
    }

    public void warn(String message, Object... var) {
        if (message.contains("{}"))
            log(message.replace("{}", var[0].toString()), WARNING);
        else {
            for (int i = 0; i < var.length; i++)
                log(message.replace("{" + i + "}", "" + var[i]), WARNING);
        }
    }

    public void error(String message) {
        log(message, ERROR);
    }

    public void error(String message, Throwable error) {
        log(message + "\n" + error, ERROR);
    }

    public void error(String message, Object var, Throwable error) {
        log(message.replace("{}", var.toString()) + "\n" + error, ERROR);
    }

    public void error(String message, Object... var) {
        if (message.contains("{}"))
            log(message.replace("{}", var[0].toString()), ERROR);
        else {
            for (int i = 0; i < var.length; i++)
                log(message.replace("{" + i + "}", "" + var[i]), ERROR);
        }
    }

    private void log(String message, LogLevel level) {
        Throwable th = new Throwable();
        StackTraceElement t = th.getStackTrace()[2];
        if (hyperlink) {
            //  if (t.getFileName() == null)
            //  t = StackTraceUtils.deepSanitize(th).getStackTrace()[3];
            message = ".(" + t.getFileName() + ":" + t.getLineNumber() + ") " + message;
        } else
            message = "(" + t.getClassName() + ") " + message;
        message = level.toString() + " " + message;
        if (showThreadName)
            message = Thread.currentThread().getName() + " " + message;
        if (showDateTime)
            message = "[" + dateFormat.format(new Date()) + "] " + message;
        if (level == ERROR || level == WARNING)
            System.err.println(message);
        else
            System.out.println(message);
    }

    public Logger setDateFormat(String dateFormat) {
        this.dateFormat = new SimpleDateFormat(dateFormat);
        return this;
    }

    public Logger setShowDateTime(boolean showDateTime) {
        this.showDateTime = showDateTime;
        return this;
    }

    public Logger setShowThreadName(boolean showThreadName) {
        this.showThreadName = showThreadName;
        return this;
    }

    public Logger setHyperlink(boolean hyperlink) {
        this.hyperlink = hyperlink;
        return this;
    }

    public enum LogLevel {
        ERROR, INFO, DEBUG, WARNING;

        @Override
        public String toString() {
            String name = this.name();
            String total = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
            return "[" + total + "]";
        }
    }
}