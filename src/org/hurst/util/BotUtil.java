package org.hurst.util;

/**
 * Created by tyler on 12/15/2016.
 */
public class BotUtil {
    public static String bold(String string) {
        return "**" + string + "**";
    }

    public static String italics(String string) {
        return "*" + string + "*";
    }

    public static String boldItalics(String string) {
        return "***" + string + "***";
    }

    public static String strikeout(String string) {
        return "~~" + string + "~~";
    }

    public static String underline(String string) {
        return "__" + string + "__";
    }

    public static String underlineItalics(String string) {
        return "__*" + string + "*__";
    }

    public static String underlineBold(String string) {
        return "__**" + string + "**__";
    }

    public static String underlineBoldItalics(String string) {
        return "__***" + string + "***__";
    }

    public static String lightCode(String string) {
        return "`" + string + "`";
    }

    public static String block(String string) {
        return "```" + string + "```";
    }

    public static String green(String string) {
        return "```css\n" + string + "\n```";
    }

    public static String orange(String string) {
        return "```http\n" + string + "\n```";
    }
}
