package org.hurst.util;

import java.util.*;

/**
 * Created by tyler on 12/14/2016.
 */
public class ObjectUtil {
    @SafeVarargs
    public static <T> void requireNonNull(T... obj) {
        for (T t : obj)
            Objects.requireNonNull(t);
    }
}
