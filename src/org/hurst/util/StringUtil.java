package org.hurst.util;

import java.text.*;
import java.util.*;

/**
 * @author Tyler Hurst
 */
public class StringUtil {
    private static final NumberFormat FORMAT = NumberFormat.getInstance();

    public static String combine(String[] stringArray) {
        StringBuilder builder = new StringBuilder();
        Arrays.stream(stringArray).forEach(builder::append);
        return builder.toString();
    }

    public static String format(Object value) {
        try {
            return FORMAT.format(value);
        } catch (IllegalArgumentException e) {
            try {
                return FORMAT.format(Integer.parseInt((String) value));
            } catch (Exception e1) {
                e1.printStackTrace();
                return null;
            }
        }
    }

    public static String getIndefiniteArticle(String string) {
        char first = Character.toLowerCase(string.charAt(0));
        if (isUppercase(string) && (first == 'f' || first == 'l' || first == 'm' || first == 'n' || first == 's'))
            return "an";
        boolean vowel = first == 'a' || first == 'e' || first == 'i' || first == 'o' || first == 'u';
        return (vowel ? "an" : "a") + " " + string;
    }

    private static boolean isUppercase(String string) {
        for (char character : string.toCharArray()) {
            if (Character.isLowerCase(character))
                return false;
        }
        return true;
    }

    public static String pluralize(String base, Number value) {
        return value.longValue() > 1 ? base + "s" : base;
    }

    public static String formatDisplayName(String name) {
        Objects.requireNonNull(name);
        name = name.replaceAll("_", " ").toLowerCase();
        StringBuilder builder = new StringBuilder();
        boolean spaced = true;
        for (int i = 0; i < name.length(); i++) {
            if (spaced) {
                builder.append(("" + name.charAt(i)).toUpperCase());
                spaced = false;
            } else
                builder.append(name.charAt(i));
            if (name.charAt(i) == ' ')
                spaced = true;
        }
        return builder.toString();
    }
}