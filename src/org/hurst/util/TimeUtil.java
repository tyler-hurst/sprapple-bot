package org.hurst.util;

import java.text.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by tyler on 7/20/2016.
 */
public class TimeUtil {
    private static final SimpleDateFormat prettyFormat = new SimpleDateFormat("hh:mm");
    private static long previous, delta;

    public static long currentTimeMillis() {
        long time = System.currentTimeMillis();
        if (time < previous)
            delta += (previous - time);
        return (previous = time) + delta;
    }

    public static String getGameTime() {
        return prettyFormat.format(currentTimeMillis());
    }

    public static boolean isWeekend() {
        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        return day == Calendar.FRIDAY || day == Calendar.SATURDAY || day == Calendar.SUNDAY;
    }

    public static String prettyTimeDuration(long ms) {
        long nanos = TimeUnit.MILLISECONDS.toNanos(ms);
        TimeUnit unit = chooseUnit(nanos);
        double value = (double) nanos / (double) TimeUnit.NANOSECONDS.convert(1L, unit);
        return String.format(Locale.ROOT, "%.4g %s", value, abbreviate(unit));
    }

    private static TimeUnit chooseUnit(long nanos) {
        return TimeUnit.DAYS.convert(nanos, TimeUnit.NANOSECONDS) > 0L ? TimeUnit.DAYS : (TimeUnit.HOURS.convert(nanos,
                TimeUnit.NANOSECONDS) > 0L ? TimeUnit.HOURS : (TimeUnit.MINUTES.convert(nanos,
                TimeUnit.NANOSECONDS) > 0L ? TimeUnit.MINUTES : (TimeUnit.SECONDS.convert(nanos,
                TimeUnit.NANOSECONDS) > 0L ? TimeUnit.SECONDS : (TimeUnit.MILLISECONDS.convert(nanos,
                TimeUnit.NANOSECONDS) > 0L ? TimeUnit.MILLISECONDS : (TimeUnit.MICROSECONDS.convert(nanos,
                TimeUnit.NANOSECONDS) > 0L ? TimeUnit.MICROSECONDS : TimeUnit.NANOSECONDS)))));
    }

    private static String abbreviate(TimeUnit unit) {
        switch (unit.ordinal()) {
            case 0:
                return "nanoseconds";
            case 1:
                return "microseconds";
            case 2:
                return "milliseconds";
            case 3:
                return "seconds";
            case 4:
                return "minutes";
            case 5:
                return "hours";
            case 6:
                return "days";
            default:
                throw new AssertionError();
        }
    }

}