package org.hurst.discord.command;

/**
 * Created by tyler on 12/15/2016.
 */
public enum PrivilegeLevel {
    PERMISSIONS_REMOVED, DEFAULT_PERMISSIONS, BOT_MANAGER;
}
